package org.gitcoolstuff.proxy.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.gitcoolstuff.proxy.domain.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(value = {"/environment.js"}, method = RequestMethod.GET)
    public @ResponseBody String environment(HttpServletRequest request) {

        Environment environment = new Environment();
        environment.getEndpoints().setServerProxy(request.getContextPath());

        try {
            String environmentJson = objectMapper.writeValueAsString(environment);
            return "window.environment =" + environmentJson + ";";
        } catch (JsonProcessingException e) {
            return "window.environment ={\"error\": \"json processing error\"};";
        }
    }

}
