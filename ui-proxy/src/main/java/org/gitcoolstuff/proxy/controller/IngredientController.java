package org.gitcoolstuff.proxy.controller;

import org.gitcoolstuff.proxy.service.IngredientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.gitcoolstuff.proxy.entity.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ingredient")
public class IngredientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientController.class);

    @Autowired
    private IngredientService ingredientService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<Ingredient> getAllIngedients() {
        LOGGER.debug("getAllIngredients - TODO");
        return ingredientService.getAll();
    }

}
