package org.gitcoolstuff.proxy.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Environment implements Serializable {

    private Endpoints endpoints = new Endpoints();

    public Endpoints getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Endpoints endpoints) {
        this.endpoints = endpoints;
    }

    public class Endpoints{

        @JsonProperty("server")
        private String serverProxy;

        public String getServerProxy() {
            return serverProxy;
        }

        public void setServerProxy(String serverProxy) {
            this.serverProxy = serverProxy;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("serverProxy", serverProxy)
                    .toString();
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("endpoints", endpoints)
                .toString();
    }
}
