INSERT INTO PERSON (
  ID,
  TITLE,
  FIRST_NAME,
  MIDDLE_NAME,
  LAST_NAME,
  SUFFIX,
  DATE_OF_BIRTH
) VALUES (
  'f14c24899e6a49ce851b3820b5d12268',
  NULL,
  'Joshua',
  'Thomas',
  'Pyle',
  'Sr',
  {ts '1974-04-21'}
);