angular.module('App').config(function ($routeProvider) {

  $routeProvider.when('/home', {templateUrl: 'app/components/home/home-view.html'});
  $routeProvider.when('/report', {templateUrl: 'app/components/report/report-view.html'});

  /* Add New Routes Above */
  $routeProvider.otherwise({redirectTo: '/home'});

});
