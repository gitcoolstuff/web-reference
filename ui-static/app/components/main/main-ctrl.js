angular.module('App').controller('MainCtrl', function MainCtrl($scope, $mdSidenav, $log) {
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId)
      .toggle()
      .then(function(){
        $log.info('mdSideNav - toggled show/hide');
      });
  };
});
