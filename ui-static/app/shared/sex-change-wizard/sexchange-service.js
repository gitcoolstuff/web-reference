angular.module('App').service('SexChange', function SexChange($mdDialog) {

	return {
		show: function show(name) {
			$mdDialog.show({
				parent: angular.element(document.body),
				templateUrl: 'app/shared/sex-change-wizard/sexchange-view.html',
				controller: 'SexChangeCtrl',
				locals: {
					currentName: name || undefined
				}
			});
		}
	};
});