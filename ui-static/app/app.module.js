angular.module('App', ['ngRoute', 'ngCookies', 'ngAnimate', 'ngResource', 'ngMaterial', 'ngMessages'])
  .constant("environment", window.environment)

  .config(function config($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('light-blue', {'default': '800'})
      .accentPalette('blue', {'default': '900'})
      .warnPalette('red')
      .backgroundPalette('grey');
  })

  .run(function run($rootScope) {

    $rootScope.displayDate = function displayDate(dateTime) {
      return moment(dateTime).format('llll');
    };

    $rootScope.safeApply = function safeApply(fn) {
      var phase = $rootScope.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };

    $rootScope.onVisible = function onVisible(queryItems, callback) {

      var elements = [];
      var that = this;
      var unregister;
      var restParams = Array.prototype.slice.call(arguments, 2);

      if (queryItems instanceof Array) {
        for (var i = 0, iLen = queryItems.length; i < iLen; ++i) {
          elements.push(queryItems[i] + ':visible');
        }
      } else {
        elements.push(queryItems + ':visible');
      }

      unregister = this.$watch(function watchCondition() {

        for (var i = 0, iLen = elements.length; i < iLen; ++i) {
          if (angular.element(elements[i]).length <= 0) {
            return false;
          }
        }

        return true;

      }, function onChange(newValue) {
        if (newValue) {
          unregister();
        }

        if (typeof callback === 'function') {
          return callback.apply(that, restParams);
        }
      });
    };
  });