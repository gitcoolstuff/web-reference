package org.gitcoolstuff.selenium.steps;

import net.thucydides.core.annotations.Step;
import org.gitcoolstuff.selenium.pages.HomePage;
import org.openqa.selenium.WebElement;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class HomeSteps extends BaseSteps {

    HomePage homePage;

    @Step
    public void openHomePage() {
        homePage.open();
        waitForAngular();
    }

    @Step("Click the 'Hello There!' Button")
    public void clickHelloThereButton() {
        WebElement helloButton = homePage.getHelloButton();
        helloButton.click();
    }

    @Step
    public String getTheAlertDialogTitle() {
        WebElement alertHeaderElement = homePage.getAlertDialogH2();
        assertThat(alertHeaderElement.isDisplayed(), is(equalTo(true)));
        return alertHeaderElement.getText();
    }

}
